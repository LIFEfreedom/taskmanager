#include <iostream>
#include <fstream>
#include <string>
#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>
#include <shellapi.h>

using namespace std;

// ����� ���� ����������� ������� ��� ID ��������
void PrintModules (DWORD const dwProcessID, ofstream &out);

// ���������� ������ ���� � ������������ ����� ��������, ���� �������� ������ � ����� ��������
string GetFullProcessName (DWORD const dwProcessID);

// ���������� �������� �� ������� Wow64-��������� (�.�. 32-������ ����������� � 64-������ ������������ �������)
bool IsWow64 (DWORD dwProcessID);

int main (int argc, char* argv[])
{
	// ��������� �������� ����� � �������
	setlocale (LC_ALL, "Russian");

	// ����� ��������� �����
	ofstream fout;

	// ��������/�������� �����, ������ ���������� ����������� �����
	fout.open ("TaskManager.txt", ios_base::trunc);

	HANDLE hSnap = 0;			// ��������� �� ������ �������
	PROCESSENTRY32 pe32;		// ���������� � ��������

	TOKEN_PRIVILEGES tp;
	LUID luidDebug;
	HANDLE hToken;
	
	// ��������� ���������� �� ������ � ��������� ���������
	// �������� Token'� �������� ��������
	if (OpenProcessToken (GetCurrentProcess (), TOKEN_ADJUST_PRIVILEGES, &hToken) != 0)
	{
		// ��������� ����������� �������������� �� ���������� �������
		if (LookupPrivilegeValue (NULL, SE_DEBUG_NAME, &luidDebug) != 0)
		{
			tp.PrivilegeCount = 1;
			tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
			tp.Privileges[0].Luid = luidDebug;

			// ���������� ����������
			if (AdjustTokenPrivileges (hToken, 0, &tp, sizeof (TOKEN_PRIVILEGES), 0, 0) == 0)
			{
				// ����������, � ������ ������ ����� ����������
				cout << "�� ������� �������� �������������� ����������. ������ ���������� ����� �������� �� ��� ���� ���������.";
				system ("pause");
			}
		}
	}
	
	// ��������� ������ ���������
	hSnap = CreateToolhelp32Snapshot (TH32CS_SNAPPROCESS, 0);

	if (hSnap != INVALID_HANDLE_VALUE)
	{
		pe32.dwSize = sizeof (PROCESSENTRY32);
		// ��������� ������� �������� � ������
		if (Process32First (hSnap, &pe32) != FALSE)
		{
			// ����� �������������� ����� �������
			// ����� ���������
			fout.width (12);
			fout << "ID ��������|";
			// ������ 90 �� ������, ����� ���������� �������� �� ��������
			fout.width (90);
			fout << "";
			fout.setf (ios::left);
			fout.width (90);
			fout << "������ ���� � �����" << "|";

			fout.width (15);
			fout << "���-�� �������" << "|";

			fout.width (12);
			fout << "����������|" << "\n";
		
			string fullPath;		// ������ ���� � ������������ �����
			
			// ��������� ��������� ���������� ���������
			while (Process32Next (hSnap, &pe32))
			{
				// ��������� ������� ���� � ����������� ��������
				fullPath = GetFullProcessName (pe32.th32ProcessID);
				// ����� ������ � ��������� ��������
				// ID ��������
				fout << "   "; fout.width (8);					fout << pe32.th32ProcessID << "|";
				// ������ ���� �� ��������									���� ������� ������� "", �� ������� ��� ������������ �����
				fout.setf (ios::left);				fout.width (180); fout << ((fullPath == "") ? pe32.szExeFile : fullPath) << "|";
				// ���������� �������
				fout << "   "; fout.width (12);		fout << pe32.cntThreads << "|";
				// ���������� � ������
				fout.width (12);					fout << ((IsWow64 (pe32.th32ProcessID) == TRUE) ? "(32 bit)" : "") << "\n";
				// ����� ���� ���������� �������
				PrintModules (pe32.th32ProcessID, fout);
			}
		}
	}

	// �������� ������ �����
	fout.close ();

	// �������� ���������� ����� � ��������� ��������� ��� ��������� ����������� ���������� ���������
	ShellExecute (0, "open", "TaskManager.txt", 0, 0, SW_SHOWNORMAL);

	// �������� ������������
	CloseHandle (hSnap);

	CloseHandle (hToken);

	return 0;
}

// ����� ���� ����������� ������� ��������
void PrintModules (DWORD dwProcessID, ofstream &out)
{
	MODULEENTRY32 me32;			// ���������� � ������
	
	// ��������� ������ �������
	HANDLE hShot = CreateToolhelp32Snapshot (TH32CS_SNAPMODULE, dwProcessID);

	// ���� ������ �������� �� �������, �� ������ �� ������ � ������� �� �������
	if (hShot == INVALID_HANDLE_VALUE)
	{
		cout << "�� ������� �������� ���������� � ������� �������� ID:" << dwProcessID << endl;
		return;
	}

	// ��������� ������� ������
	Module32First (hShot, &me32);

	out << "����������� ������:";

	do
	{
		// ����� ������������ ������
		out << me32.szModule << "\n";
		out.width (19); out << "";		// ������ ��� ����������
	} while (Module32Next (hShot, &me32));

	out << "\n";

	// �������� ������
	CloseHandle (hShot);
}

// ��������� ������� ���� � ��������
string GetFullProcessName (DWORD const dwProcessID)
{
	char str[MAX_PATH] = { 0 };

	// �������� �������� ��� ��������� ���������� � ���
	HANDLE proc = OpenProcess (PROCESS_QUERY_LIMITED_INFORMATION, FALSE, dwProcessID);

	DWORD size = 256;

	if (QueryFullProcessImageName (proc, 0, str, &size) == 0)
	{
		cout << "�� ������� �������� ������ ���� � �������� ID:" << dwProcessID << endl;
		system ("pause");
	}

	string result (str);

	return result;
}

// ����������� ����������� ��������
bool IsWow64 (DWORD dwProcessID)
{
	BOOL Isx64 = FALSE;

	// ����������� ��������� �� �������
	typedef BOOL (APIENTRY *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

	LPFN_ISWOW64PROCESS fnIsWow64Process;

	// ��������� ��������� ������ �������
	fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress (GetModuleHandle ("kernel32"), "IsWow64Process");

	// �������� �������� ��� ������ ���������� �� ����
	HANDLE process = OpenProcess (PROCESS_QUERY_LIMITED_INFORMATION, FALSE, dwProcessID);

	if (fnIsWow64Process != 0)
	{
		// ���� ����� ������� ����������, �� �������� � �����
		if (fnIsWow64Process (process, &Isx64) == 0)
		{
			cout << "�� ������� �������� ����������� ��������" << endl;
		}
	}

	return Isx64 != FALSE;
}